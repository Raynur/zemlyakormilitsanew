﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace zemlyaKormilitsa
{
	[Activity (Label = "AInformationInOneActivity")]			
	public class AInformationInOneActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			RequestWindowFeature (WindowFeatures.NoTitle);
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.LListViewInformationInOneLayout);

			//string lineForSQL = Intent.GetStringExtra ("Button");//Intent.GetStringExtra ("SightName");

			SQLiteRequest requestSQL = new SQLiteRequest ("SELECT * FROM" + Intent.GetStringExtra ("Button"));

			List <PlantClass> plantsList = requestSQL.GetItems(); //ЭТО ПРАВИЛЬНО?



			LinearLayout left = FindViewById<LinearLayout>(Resource.Id.left);
			LinearLayout right = FindViewById<LinearLayout> (Resource.Id.right);

			for (int i = 0; i < plantsList.Count; i++) {
				LinearLayout second = new LinearLayout (this);
				second.Orientation = Orientation.Vertical;

				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams (ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
				lp.Width = Resources.DisplayMetrics.WidthPixels / 2;

				second.LayoutParameters = lp;

				ImageView image1 = new ImageView (this);
				image1.SetBackgroundResource (Resource.Drawable.Icon);//something 
				second.AddView (image1);
				TextView text1 = new TextView (this);
				text1.Text = plantsList[i].name;
				second.AddView (text1);

				left.AddView(second);


			}




		}
		protected override void OnPause ()
		{
			base.OnPause ();
			System.GC.Collect();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			System.GC.Collect();
		}
	}
}

