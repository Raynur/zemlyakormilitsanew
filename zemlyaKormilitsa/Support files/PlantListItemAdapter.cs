﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace zemlyaKormilitsa
{
	[Activity (Label = "PlantListItemAdapter")]			
	public class PlantListItemAdapter : BaseAdapter<PlantClass>
	{
		
			public List<PlantClass> items; // Передаваемый список достопримечательностей
			Activity context; // Необходимая информация о контексте выполнения

		  	public PlantListItemAdapter (Activity context, List<PlantClass> items) // Стандартный конструктор для инициализации адаптера
			{
				this.context = context;
				this.items = items;
			}
			public override long GetItemId(int position) // Получение номера элемента(стандартная, необходимая конструкция)
			{
				return position;
			}
			public override PlantClass this[int position]// Получение информации о нажатой достопримечательности(стандартная, необходимая конструкция)
			{
				get { return items[position]; }
			}
			public override int Count // Получение количества элементов в списке(стандартная, необходимая конструкция)
			{
				get { return items.Count; }
			}
			public override View GetView(int position, View convertView, ViewGroup parent) // Заполнение элемента интерфейса типа список.
		{
			var item = items [position]; //Выделение отдельного элемента списка
			View view = convertView;

			if (view == null) // no view to re-use, create new
					view = context.LayoutInflater.Inflate (Resource.Layout.LPlantListItemAdapter, null); // Подключение к интерфейсу кастомного элемента интерфейсного списка
			view.FindViewById<TextView> (Resource.Id.textView1).Text = item.name; // Присваивание первому текстовому полю названия достопримечательности
			//view.FindViewById<TextView>(Resource.Id.textView2).Text = item.adress; // Присваивание второму текст. полю адреса достопримечательности
			view.FindViewById<ImageView> (Resource.Id.imageView1).SetImageResource (item.imageId); // Вставка изображения

			return view;
		}
	}
}

