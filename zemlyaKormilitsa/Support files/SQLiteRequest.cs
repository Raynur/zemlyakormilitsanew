﻿using System;
using Mono.Data.Sqlite;
using System.Data;
using System.Collections.Generic;

namespace zemlyaKormilitsa
{
	public class SQLiteRequest
	{
		public String SQL_Request;
		
		public SQLiteRequest (String SQL_Request)
		{
			this.SQL_Request = SQL_Request;
		}


		public List<PlantClass> GetItems(){
			List <PlantClass> plantItems = new List<PlantClass>();

			string dbPath = "";

			Mono.Data.Sqlite.SqliteConnection connection = new SqliteConnection ("Data source=" + dbPath);
			Mono.Data.Sqlite.SqliteCommand command = connection.CreateCommand ();



			connection.Open ();
			command.CommandText = SQL_Request;
			Mono.Data.Sqlite.SqliteDataReader reader = command.ExecuteReader ();


			while(reader.Read()){
				plantItems.Add(new PlantClass (reader [0].ToString (),  reader [1].ToString(), int.Parse(reader [2].ToString())));

			}
		


			reader.Close ();

			connection.Close ();

			return plantItems;

		}
	}
}

